package tasks.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

class TasksServiceRepoIntegrationTest {

    private TasksService service;
    private ArrayTaskList repo;
    private Task task;

    @BeforeEach
    void setUp() {
        repo = new ArrayTaskList();
        service = new TasksService(repo);

        task = mock(Task.class);
    }

    @Test
    public void Test_01_getObservableList_with_elements(){
        repo.add(task);
        assert service.getObservableList().size() == 1;
    }

    @Test
    public void Test_02_getObservableList_without_elements() {
        repo.add(task);
        repo.add(task);
        repo.add(task);
        repo.add(task);
        repo.add(task);
        repo.remove(task);

        assert service.getObservableList().size() == 4;
    }
}