package tasks.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class TasksServiceTestWithMockConstructor {

    private TasksService service;
    private ArrayTaskList repo;
    private Task task;
    private int timeUnit;
    private String resTime = "10";
    private int resSeconds = 1800;
    private String stringtime = "00:30";

    @BeforeEach
    public void setUp() {
        repo = mock(ArrayTaskList.class);
        service = mock(TasksService.class);
        timeUnit = 10;
    }
    @Test
    public void Test_03_getObservable(){
        TasksService serv = new TasksService(repo);
        task = mock(Task.class);

        Mockito.when(repo.getAll()).thenReturn(Arrays.asList(task));
        Mockito.doNothing().when(repo).add(task);
        repo.add(task);
        Mockito.verify(repo,times(1)).add(task);

        assert serv.getObservableList().size() == 1;
    }
    @Test
    public void Test_01_getInterval(){
        Mockito.when(service.formTimeUnit(timeUnit)).thenReturn(resTime);
        String rez = service.formTimeUnit(timeUnit);
        Mockito.verify(service, times(1)).formTimeUnit(timeUnit);

        assert rez == resTime;
    }

    @Test
    public void Test_02_ParseFromStringToSeconds(){
        Mockito.when(service.parseFromStringToSeconds(stringtime)).thenReturn(resSeconds);

        int rez = service.parseFromStringToSeconds(stringtime);

        Mockito.verify(service, times(1)).parseFromStringToSeconds(stringtime);

        assert rez == resSeconds;
    }

}