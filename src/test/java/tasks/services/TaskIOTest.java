package tasks.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;

import tasks.model.*;

import static org.junit.jupiter.api.Assertions.*;

class TaskIOTest {
    private TaskList tasks;
    private File file;
    private TaskList arr;

    @BeforeEach
    public void setUp() throws Exception {
        tasks = new ArrayTaskList();
        arr = new ArrayTaskList();

        Task task = new Task("Ana1",new Date());
        tasks.add(task);
        task = new Task("Ana2",new Date());
        tasks.add(task);
        task = new Task("Ana3",new Date());
        tasks.add(task);
        task = new Task("Ana4",new Date());
        tasks.add(task);
        task = new Task("Ana5",new Date());
        tasks.add(task);
        task = new Task("Ana6",new Date());
        tasks.add(task);
        task = new Task("Ana7",new Date());
        tasks.add(task);
        task = new Task("Ana8",new Date());
        tasks.add(task);
        task = new Task("Ana9",new Date());
        tasks.add(task);
        task = new Task("Ana10",new Date());
        tasks.add(task);
        task = new Task("Ana11",new Date());
        tasks.add(task);
        task = new Task("Ana12",new Date());
        tasks.add(task);
        task = new Task("Ana13",new Date());
        tasks.add(task);
        task = new Task("Ana14",new Date());
        tasks.add(task);
    }

    @Test
    @DisplayName("Caz general write")
    void writeText() {
        file = new File("test1.txt");

        try {
            TaskIO.writeText(tasks, file);
            TaskIO.readText(arr,file);
            assert( tasks.size() == arr.size());
        }
        catch (IOException e){
            assert false;
        }
    }

    @Test
    @DisplayName("Caz in care fisier e null")
    void writeTextBadFile() {
        file = new File("");

        try {
            TaskIO.writeText(tasks, file);
            assert false;
        }
        catch (IOException e){
            assert true;
        }
    }

    @Test
    @DisplayName("Caz in care fisierul nu exista")
    void writeTextNoFile() {
        file = new File("nofile.txt");

        try {
            Files.deleteIfExists(file.toPath());

            TaskIO.writeText(tasks, file);
            TaskIO.readText(arr,file);
            assert( tasks.size() == arr.size());
        }
        catch (IOException e){
            assert false;
        }
    }

    //reparat cod in TaskIO.java, rand 109
    @Test
    @DisplayName("Caz in care lista e goala")
    void writeTextEmptyList() {
        file = new File("test2.txt");
        tasks = new ArrayTaskList();

        try {
            TaskIO.writeText(tasks, file);
            TaskIO.readText(arr,file);
            assert (arr.size() == 0);
            assert (tasks.size() == arr.size());
        }
        catch (IOException e){
            assert false;
        }
    }
}