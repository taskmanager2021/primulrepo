package tasks.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TasksServiceWithRepoAndEntityIntegration {

    private TasksService service;
    private ArrayTaskList repo;
    private Task task;

    @BeforeEach
    void setUp() {
        repo = new ArrayTaskList();
        service = new TasksService(repo);
    }

    @Test
    public void test_01_add_multiple_entity(){
        task = new Task("Ana1",new Date());
        repo.add(task);
        task = new Task("Ana2",new Date());
        repo.add(task);
        task = new Task("Ana3",new Date());
        repo.add(task);
        task = new Task("Ana4",new Date());
        repo.add(task);
        task = new Task("Ana5",new Date());
        repo.add(task);
        task = new Task("Ana6",new Date());
        repo.add(task);

        assert service.getObservableList().size() == 6;
    }

    @Test
    public void test_02_add_invalid(){
        try {
            task = null;
            repo.add(task);
        }
        catch (NullPointerException e) {
            assert service.getObservableList().size() == 0;
        }
    }
}