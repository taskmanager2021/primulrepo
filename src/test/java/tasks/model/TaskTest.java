package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {
    private String titlu;
    SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat data2 = new SimpleDateFormat("dd/MM/yyyy hh:mm");
    Task task;

    @BeforeEach
    public void setUp() throws Exception {
        titlu = "Ana";
    }

    @Test
    @DisplayName("Caz general task1")
    void createTask() {
        try{
            task = new Task(titlu, new Date());
            assert true;
        }
        catch (IllegalArgumentException e){
            assert false;
        }
    }

    @Test
    @DisplayName("Caz data out of bounds < 1970, task1")
    void createTaskWrongDate() {
        try{
            task = new Task(titlu, data.parse("31/12/1969"));
            assert false;
        }
        catch (IllegalArgumentException e){
            assert true;
        }
        catch (ParseException e){
            assert false;
        }
    }

    @Test
    @DisplayName("Caz data si ora, task1")
    void createTaskWrongDateAndTime() {
        try{
            task = new Task(titlu, data2.parse("31/12/10000 67:67"));
            System.out.println(data2.parse("31/12/10000 67:67"));
            assert true;
        }
        catch (IllegalArgumentException e){
            assert false;
        }
        catch (ParseException e){
            assert false;
        }
    }

    @Test
    @DisplayName("Caz general task2")
    void createTask2() {
        try{
            task = new Task(titlu, data.parse("31/12/1999"), data.parse("31/12/1999"), 2);
            assert true;
        }
        catch (IllegalArgumentException e){
            assert false;
        }
        catch (ParseException e){
            assert false;
        }
    }


    @Test
    @DisplayName("Caz data out of bounds < 1970, task2")
    void createTaskWrongDate2() {
        try{
            task = new Task(titlu, data.parse("31/12/1969"), data.parse("31/12/1965"), 2);
            assert false;
        }
        catch (IllegalArgumentException e){
            assert true;
        }
        catch (ParseException e){
            assert false;
        }
    }

    @Test
    @DisplayName("Caz start > end, task2")
    void createTaskGreaterStart() {
        try{
            task = new Task(titlu, data.parse("31/12/2069"), data.parse("31/12/2065"), 2);
            assert false;
        }
        catch (IllegalArgumentException e){
            assert true;
        }
        catch (ParseException e){
            assert false;
        }
    }

    //reparat cod in Task.java, linia 35
    @Test
    @DisplayName("Caz interval < 1, task2")
    void createTaskBadInterval() {
        try{
            task = new Task(titlu, data.parse("31/12/2011"), data.parse("31/12/2012"), 0);
            assert false;
        }
        catch (IllegalArgumentException e){
            assert true;
        }
        catch (ParseException e){
            assert false;
        }
    }
}