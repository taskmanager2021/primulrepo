package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskTestWBT {
    Task task;
    Task taskInterval;
    SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat data2 = new SimpleDateFormat("dd/MM/yyyy hh:mm");


    // public Task(String title, Date start, Date end, int interval)

    @BeforeEach
    public void setUp() throws Exception {
        task = new Task("Titlu", data.parse("26/04/2021"));
        task.setActive(true);
        taskInterval = new Task("Titlu", data.parse("20/04/2021"), data.parse("26/04/2021"), 86400);
        taskInterval.setActive(true);

    }

    @Test
    @DisplayName("Task before time")
    void taskBeforeTime(){
        Date current = null;
        Date expected = null;

        try {
            current = data.parse("25/04/2021");
            expected = data.parse("26/04/2021");
        }
        catch (ParseException pe){
            assert false;
        }
            Date rez = task.nextTimeAfter(current);
        assert (rez.equals(expected));
    }

    @Test
    @DisplayName("Task inactive")
    void taskBeforeTimeInactive(){
        Date current = null;
        Date expected = null;
        task.setActive(false);
        try {
            current = data.parse("25/04/2021");
            expected = data.parse("26/04/2021");
        }
        catch (ParseException pe){
            assert false;
        }
        Date rez = task.nextTimeAfter(current);

        assert (rez == null);
    }

    @Test
    @DisplayName("Date after end")
    void taskBeforeTimeAfterEnd(){
        Date current = null;
        try {
            current = data.parse("30/04/2021");
        }
        catch (ParseException pe){
            assert false;
        }

        Date rez = taskInterval.nextTimeAfter(current);

        assert (rez == null);
    }

    @Test
    @DisplayName("Date before start in Interval")
    void taskBeforeTimeBeforeStart(){
        Date current = null;
        Date expected = null;
        try {
            current = data.parse("15/04/2021");
            expected = data.parse("20/04/2021");
        }
        catch (ParseException pe){
            assert false;
        }

        Date rez = taskInterval.nextTimeAfter(current);


        assert (rez.equals(expected));
    }

    @Test
    @DisplayName("Current is in interval")
    void taskBeforeTimeInInterval(){
        Date current = null;
        Date expected = null;
        try {
            current = data.parse("22/04/2021");
            expected = data.parse("23/04/2021");
        }
        catch (ParseException pe){
            assert false;
        }

        Date rez = taskInterval.nextTimeAfter(current);

        assert (rez.equals(expected));
    }

    @Test
    @DisplayName("Current is between interval")
    void taskBeforeTimeBetweenInterval(){
        Date current = null;
        Date expected = null;
        try {
            current = data2.parse("22/04/2021 11:00");
            expected = data2.parse("23/04/2021 00:00");
        }
        catch (ParseException pe){
            assert false;
        }

        Date rez = taskInterval.nextTimeAfter(current);

        assert (rez.equals(expected));
    }


}