package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.services.TasksService;

import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ArrayTaskListWithMockConstructor {

    private ArrayTaskList repo;
    private Task task;
    private Task tasknull;

    @BeforeEach
    public void setUp() {
        task = new Task("Ana", new Date());
        tasknull = null;
        repo = mock(ArrayTaskList.class);
    }

    @Test
    public void Test_01_add_valid_task(){
        Mockito.when(repo.getAll()).thenReturn(Arrays.asList(task));
        Mockito.doNothing().when(repo).add(task);

        repo.add(task);

        Mockito.verify(repo,times(1)).add(task);
        Mockito.verify(repo, never()).getAll();

        assert true;
    }
    @Test
    public void Test_02_add_invalid_task(){

        Mockito.doNothing().when(repo).add(task);
        Mockito.doThrow(NullPointerException.class).when(repo).add(tasknull);
        try{

            repo.add(tasknull);
            assert false;
        }
        catch (NullPointerException e){
            Mockito.verify(repo,times(1)).add(tasknull);
            Mockito.verify(repo, never()).getAll();
            assert true;
        }
    }
}